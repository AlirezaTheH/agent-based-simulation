import random

import numpy as np
from scipy.ndimage import convolve
from mesa import Model
from mesa.time import BaseScheduler
from mesa.space import SingleGrid

from models import (Wall,
                    Obstacle,
                    CityPassenger,
                    IntercityPassenger,
                    Baggage)
from utils import line_segment_length_in_grid_cell
from dtypes import (TunnelName,
                    DoorScenario,
                    Direction,
                    PassengerPathDesire,
                    TurnBufferType,
                    TurnBufferAccessType,
                    TorbType)


class SimultaneousActivation(BaseScheduler):
    """
    A scheduler to simulate the simultaneous activation of all the agents.
    """

    def step(self):
        for agent in sorted(self.agents, key=lambda a: a.S[a.pos], reverse=True):
            agent.step()
        self.steps += 1
        self.time += 1


class Simulator(Model):
    """
    Simulates arrivals and departures of passengers in a tunnel.
    """

    def __init__(self,
                 main_world,
                 left_world,
                 right_world,
                 main_world_turn_buffers,
                 left_world_turn_buffers,
                 right_world_turn_buffers,
                 door_scenario,
                 ks,
                 kd,
                 delta,
                 alpha,
                 grid_scale,
                 city_passengers_min_wall_distance,
                 intercity_passengers_min_wall_distance,
                 city_passengers_velocity,
                 intercity_passengers_velocity,
                 stairs_velocity,
                 escalators_velocity,
                 city_passengers_escalator_usage_percent,
                 intercity_passengers_escalator_usage_percent,
                 initial_city_passenger_number,
                 initial_intercity_passenger_number,
                 ea_city_passenger_number,
                 ea_intercity_passenger_number,
                 eb_city_passenger_number,
                 eb_intercity_passenger_number,
                 ec_city_passenger_number,
                 ec_intercity_passenger_number,
                 drop_baggage_in_congestion_percent,
                 *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.main_width, self.main_height = main_world.shape
        self.left_width, self.left_height = left_world.shape
        self.right_width, self.right_height = right_world.shape
        self.main_world = main_world
        self.left_world = left_world
        self.right_world = right_world
        self.main_world_passengers = 0
        self.left_world_passengers = 0
        self.right_world_passengers = 0
        self.main_world_turn_buffers = main_world_turn_buffers
        self.left_world_turn_buffers = left_world_turn_buffers
        self.right_world_turn_buffers = right_world_turn_buffers
        self.schedule = SimultaneousActivation(self)
        self.main_grid = SingleGrid(self.main_width, self.main_height, torus=False)
        self.left_grid = SingleGrid(self.left_width, self.left_height, torus=False)
        self.right_grid = SingleGrid(self.right_width, self.right_height, torus=False)
        self.free_distance = '*+elr'
        self.city_passengers_min_wall_distance = round(city_passengers_min_wall_distance / grid_scale)
        self.intercity_passengers_min_wall_distance = round(intercity_passengers_min_wall_distance / grid_scale)
        self.city_passengers_interval = round(grid_scale / city_passengers_velocity / 0.1)
        self.intercity_passengers_interval = round(grid_scale / intercity_passengers_velocity / 0.1)
        self.stairs_interval = round(grid_scale / stairs_velocity / 0.1)
        self.escalators_interval = round(grid_scale / escalators_velocity / 0.1)
        self.city_passengers_path_desire_p_list = (
                [PassengerPathDesire.escalators] * city_passengers_escalator_usage_percent
                + [PassengerPathDesire.stairs] * (100 - city_passengers_escalator_usage_percent))
        random.shuffle(self.city_passengers_path_desire_p_list)
        self.intercity_passengers_path_desire_p_list = (
                [PassengerPathDesire.escalators] * intercity_passengers_escalator_usage_percent
                + [PassengerPathDesire.stairs] * (100 - intercity_passengers_escalator_usage_percent))
        random.shuffle(self.intercity_passengers_path_desire_p_list)
        self.ea_city_passenger_number = ea_city_passenger_number
        self.eb_city_passenger_number = eb_city_passenger_number
        self.ec_city_passenger_number = ec_city_passenger_number
        self.ea_intercity_passenger_number = ea_intercity_passenger_number
        self.eb_intercity_passenger_number = eb_intercity_passenger_number
        self.ec_intercity_passenger_number = ec_intercity_passenger_number
        self.drop_baggage_or_not = [True] * drop_baggage_in_congestion_percent + \
                                   [False] * (100 - drop_baggage_in_congestion_percent)
        self.door_scenario = door_scenario
        self.entrances = '456'
        self.stairs = '+='
        self.estairs = '+'
        self.escalators = '*'
        self.exits = '3'
        self.left_exits = 'l'
        self.right_exits = 'r'
        self.entrance_a = []
        self.entrance_b = []
        self.entrance_c = []
        self.entrances_tunnel_exits = []
        self.main_grid_empties = []
        self.left_grid_empties = []
        self.right_grid_empties = []
        self.main_grid_left_exits = []
        self.main_grid_right_exits = []
        self.left_grid_left_exits = []
        self.left_grid_right_exits = []
        self.right_grid_left_exits = []
        self.right_grid_right_exits = []
        self.main_grid_walls = []
        self.left_grid_walls = []
        self.right_grid_walls = []
        self.unique_id = 0

        # Main tunnel
        self.mD = np.zeros_like(self.main_world, dtype=float)
        for i, j in np.indices(self.main_world.shape).reshape(2, -1).T:
            if self.main_world[i, j] == '1':
                Wall((i, j), self, self.main_grid)
                self.main_grid_walls.append((i, j))

            elif self.main_world[i, j] == '2':
                Obstacle((i, j), self, self.main_grid)
                self.main_grid_walls.append((i, j))

            elif self.main_world[i, j] == 'l':
                self.main_grid_left_exits.append((i, j))

            elif self.main_world[i, j] == 'r':
                self.main_grid_right_exits.append((i, j))

            elif self.main_world[i, j] == '4':
                self.entrance_a.append((i, j))

            elif self.main_world[i, j] == '5':
                self.entrance_b.append((i, j))

            elif self.main_world[i, j] == '6':
                self.entrance_c.append((i, j))

            elif self.main_world[i, j] == '0':
                self.main_grid_empties.append((i, j))

        # Left tunnel
        self.lD = np.zeros_like(self.left_world, dtype=float)
        for i, j in np.indices(self.left_world.shape).reshape(2, -1).T:
            if self.left_world[i, j] == '1':
                Wall((i, j), self, self.left_grid)
                self.left_grid_walls.append((i, j))

            elif self.left_world[i, j] == '2':
                Obstacle((i, j), self, self.left_grid)
                self.left_grid_walls.append((i, j))

            elif self.left_world[i, j] == 'l':
                self.left_grid_left_exits.append((i, j))

            elif self.left_world[i, j] == 'r':
                self.left_grid_right_exits.append((i, j))

            elif self.left_world[i, j] == '*':
                self.lD[i, j] = 1
                self.left_grid_empties.append((i, j))

            elif self.left_world[i, j] != '%':
                self.left_grid_empties.append((i, j))

        # Right tunnel
        self.rD = np.zeros_like(self.right_world, dtype=float)
        for i, j in np.indices(self.right_world.shape).reshape(2, -1).T:
            if self.right_world[i, j] == '1':
                Wall((i, j), self, self.right_grid)
                self.right_grid_walls.append((i, j))

            elif self.right_world[i, j] == '2':
                Obstacle((i, j), self, self.right_grid)
                self.right_grid_walls.append((i, j))

            elif self.right_world[i, j] == 'l':
                self.right_grid_left_exits.append((i, j))

            elif self.right_world[i, j] == 'r':
                self.right_grid_right_exits.append((i, j))

            elif self.right_world[i, j] == '*':
                self.rD[i, j] = 1
                self.right_grid_empties.append((i, j))

            elif self.right_world[i, j] != '%':
                self.right_grid_empties.append((i, j))

        self.ks = ks
        self.kd = kd
        self.delta = delta
        self.alpha = alpha
        self.md = self.get_d(self.main_world, self.main_grid_walls)
        self.ld = self.get_d(self.left_world, self.left_grid_walls)
        self.rd = self.get_d(self.right_world, self.right_grid_walls)
        self.slS, self.elS, self.srS, self.erS, self.smS, self.emS = self.calculate_statics()
        self.slP = np.exp(self.ks * self.slS) * np.exp(self.kd * self.lD)
        self.elP = np.exp(self.ks * self.elS) * np.exp(self.kd * self.lD)
        self.srP = np.exp(self.ks * self.srS) * np.exp(self.kd * self.rD)
        self.erP = np.exp(self.ks * self.erS) * np.exp(self.kd * self.rD)
        self.smP = np.exp(self.ks * self.smS) * np.exp(self.kd * self.mD)
        self.emP = np.exp(self.ks * self.emS) * np.exp(self.kd * self.mD)

        mn, ln, rn = self.divide_initial_passengers_number(initial_intercity_passenger_number)
        self.put_new_intercity_passengers_in(self.main_grid_empties,
                                             mn,
                                             self.main_grid,
                                             self.main_world,
                                             self.md,
                                             TunnelName.main)
        self.put_new_intercity_passengers_in(self.left_grid_empties,
                                             ln,
                                             self.left_grid,
                                             self.left_world,
                                             self.ld,
                                             TunnelName.left)
        self.put_new_intercity_passengers_in(self.right_grid_empties,
                                             rn,
                                             self.right_grid,
                                             self.right_world,
                                             self.rd,
                                             TunnelName.right)
        mn, ln, rn = self.divide_initial_passengers_number(initial_city_passenger_number)
        self.put_new_city_passengers_in(self.main_grid_empties,
                                        mn,
                                        self.main_grid,
                                        self.main_world,
                                        self.md,
                                        TunnelName.main)
        self.put_new_city_passengers_in(self.left_grid_empties,
                                        ln,
                                        self.left_grid,
                                        self.left_world,
                                        self.ld,
                                        TunnelName.left)
        self.put_new_city_passengers_in(self.right_grid_empties,
                                        rn,
                                        self.right_grid,
                                        self.right_world,
                                        self.rd,
                                        TunnelName.right)

    def step(self):
        n = 1
        n = n if self.ea_intercity_passenger_number - n >= 0 else self.ea_intercity_passenger_number
        if n > 0:
            self.ea_intercity_passenger_number -= self.put_new_intercity_passengers_in(self.entrance_a,
                                                                                       n,
                                                                                       self.main_grid,
                                                                                       self.main_world,
                                                                                       self.md,
                                                                                       TunnelName.main)
        n = 1
        n = n if self.ea_city_passenger_number - n >= 0 else self.ea_city_passenger_number
        if n > 0:
            self.ea_city_passenger_number -= self.put_new_city_passengers_in(self.entrance_a,
                                                                             n,
                                                                             self.main_grid,
                                                                             self.main_world,
                                                                             self.md,
                                                                             TunnelName.main)
        n = 1
        n = n if self.eb_intercity_passenger_number - n >= 0 else self.eb_intercity_passenger_number
        if n > 0:
            self.eb_intercity_passenger_number -= self.put_new_intercity_passengers_in(self.entrance_b,
                                                                                       n,
                                                                                       self.main_grid,
                                                                                       self.main_world,
                                                                                       self.md,
                                                                                       TunnelName.main)
        n = 1
        n = n if self.eb_city_passenger_number - n >= 0 else self.eb_city_passenger_number
        if n > 0:
            self.eb_city_passenger_number -= self.put_new_city_passengers_in(self.entrance_b,
                                                                             n,
                                                                             self.main_grid,
                                                                             self.main_world,
                                                                             self.md,
                                                                             TunnelName.main)

        n = 1
        n = n if self.ec_intercity_passenger_number - n >= 0 else self.ec_intercity_passenger_number
        if n > 0:
            self.ec_intercity_passenger_number -= self.put_new_intercity_passengers_in(self.entrance_c,
                                                                                       n,
                                                                                       self.main_grid,
                                                                                       self.main_world,
                                                                                       self.md,
                                                                                       TunnelName.main)
        n = 1
        n = n if self.ec_city_passenger_number - n >= 0 else self.ec_city_passenger_number
        if n > 0:
            self.ec_city_passenger_number -= self.put_new_city_passengers_in(self.entrance_c,
                                                                             n,
                                                                             self.main_grid,
                                                                             self.main_world,
                                                                             self.md,
                                                                             TunnelName.main)
        self.slP = np.exp(self.ks * self.slS) * np.exp(self.kd * self.lD)
        self.elP = np.exp(self.ks * self.elS) * np.exp(self.kd * self.lD)
        self.srP = np.exp(self.ks * self.srS) * np.exp(self.kd * self.rD)
        self.erP = np.exp(self.ks * self.erS) * np.exp(self.kd * self.rD)
        self.smP = np.exp(self.ks * self.smS) * np.exp(self.kd * self.mD)
        self.emP = np.exp(self.ks * self.emS) * np.exp(self.kd * self.mD)
        self.schedule.step()

        # Erasing footsteps
        x = (self.alpha * (1 - self.delta)) / 8
        kernel = np.array([[x, x, x],
                           [x, 0, x],
                           [x, x, x]])
        n = convolve(self.mD, kernel, mode='constant')
        self.mD = (1 - self.alpha) * (1 - self.delta) * self.mD + n
        n = convolve(self.lD, kernel, mode='constant')
        self.lD = (1 - self.alpha) * (1 - self.delta) * self.lD + n
        n = convolve(self.rD, kernel, mode='constant')
        self.rD = (1 - self.alpha) * (1 - self.delta) * self.rD + n

    def put_new_city_passengers_in(self, entrance, number, grid, world, d, tunnel_name):
        empties = [pos for pos in entrance if grid.is_cell_empty(pos)
                   and (world[pos] in self.free_distance
                        or d[pos] > self.city_passengers_min_wall_distance)]
        counter = 0
        for i in range(number):
            if len(empties) == 0:
                break
            pos = random.choice(empties)
            if world[pos] == '*':
                path_desire = PassengerPathDesire.escalators
            elif world[pos] == '+':
                path_desire = PassengerPathDesire.stairs
            else:
                path_desire = random.choice(self.city_passengers_path_desire_p_list)
            CityPassenger(self.unique_id, pos, self, path_desire, tunnel_name, self.city_passengers_min_wall_distance)
            empties.remove(pos)
            counter += 1

        return counter

    def put_new_intercity_passengers_in(self, entrance, number, grid, world, d, tunnel_name):
        empties = [pos for pos in entrance if grid.is_cell_empty(pos)
                   and (world[pos] in self.free_distance
                        or d[pos] > self.intercity_passengers_min_wall_distance)]
        counter = 0
        for i in range(number):
            if len(empties) == 0:
                break
            while len(empties) > 0:
                pos = random.choice(empties)
                if world[pos] == '*':
                    path_desire = PassengerPathDesire.escalators
                elif world[pos] == '+':
                    path_desire = PassengerPathDesire.stairs
                else:
                    path_desire = random.choice(self.city_passengers_path_desire_p_list)
                empty_neighbour_found = False
                for npos in grid.get_neighborhood(pos, False):
                    if npos in empties:
                        IntercityPassenger(self.unique_id,
                                           pos,
                                           Baggage(npos, self, tunnel_name),
                                           self,
                                           path_desire,
                                           tunnel_name,
                                           self.intercity_passengers_min_wall_distance)
                        empty_neighbour_found = True
                        empties.remove(npos)
                        counter += 1
                        break
                empties.remove(pos)
                if empty_neighbour_found:
                    break

        return counter

    def calculate_statics(self):
        if self.door_scenario == DoorScenario.both:
            slS = self.get_statics(Direction.left,
                                   0,
                                   0,
                                   self.left_world,
                                   self.left_world_turn_buffers.both,
                                   self.left_grid_left_exits,
                                   self.left_grid_right_exits,
                                   '12%*')
            sls_max = np.unique(slS)[-2] + 1
            elS = self.get_statics(Direction.left,
                                   0,
                                   0,
                                   self.left_world,
                                   self.left_world_turn_buffers.both,
                                   self.left_grid_left_exits,
                                   self.left_grid_right_exits,
                                   '12%+')
            els_max = np.unique(elS)[-2] + 1
            srS = self.get_statics(Direction.right,
                                   0,
                                   0,
                                   self.right_world,
                                   self.right_world_turn_buffers.both,
                                   self.right_grid_left_exits,
                                   self.right_grid_right_exits,
                                   '12%*')
            srs_max = np.unique(srS)[-2] + 1
            erS = self.get_statics(Direction.right,
                                   0,
                                   0,
                                   self.right_world,
                                   self.right_world_turn_buffers.both,
                                   self.right_grid_left_exits,
                                   self.right_grid_right_exits,
                                   '12%+')
            ers_max = np.unique(erS)[-2] + 1
            smS = self.get_statics(Direction.both,
                                   sls_max,
                                   srs_max,
                                   self.main_world,
                                   self.main_world_turn_buffers.both,
                                   self.main_grid_left_exits,
                                   self.main_grid_right_exits,
                                   '12%*')
            ss_max = np.unique(smS)[-2] + 1
            emS = self.get_statics(Direction.both,
                                   els_max,
                                   ers_max,
                                   self.main_world,
                                   self.main_world_turn_buffers.both,
                                   self.main_grid_left_exits,
                                   self.main_grid_right_exits,
                                   '12%+')
            es_max = np.unique(emS)[-2] + 1

        elif self.door_scenario == DoorScenario.only_left:
            slS = self.get_statics(Direction.left,
                                   0,
                                   0,
                                   self.left_world,
                                   self.left_world_turn_buffers.only_left,
                                   self.left_grid_left_exits,
                                   self.left_grid_right_exits,
                                   '12%*')
            sls_max = np.unique(slS)[-2] + 1
            elS = self.get_statics(Direction.left,
                                   0,
                                   0,
                                   self.left_world,
                                   self.left_world_turn_buffers.only_left,
                                   self.left_grid_left_exits,
                                   self.left_grid_right_exits,
                                   '12%+')
            els_max = np.unique(elS)[-2] + 1
            smS = self.get_statics(Direction.left,
                                   sls_max,
                                   0,
                                   self.main_world,
                                   self.main_world_turn_buffers.only_left,
                                   self.main_grid_left_exits,
                                   self.main_grid_right_exits,
                                   '12%*')
            sms_max = np.unique(smS)[-2] + 1
            emS = self.get_statics(Direction.left,
                                   els_max,
                                   0,
                                   self.main_world,
                                   self.main_world_turn_buffers.only_left,
                                   self.main_grid_left_exits,
                                   self.main_grid_right_exits,
                                   '12%+')
            ems_max = np.unique(emS)[-2] + 1
            srS = self.get_statics(Direction.left,
                                   sms_max,
                                   0,
                                   self.right_world,
                                   self.right_world_turn_buffers.only_left,
                                   self.right_grid_left_exits,
                                   self.right_grid_right_exits,
                                   '12%*')
            ss_max = np.unique(srS)[-2] + 1
            erS = self.get_statics(Direction.left,
                                   ems_max,
                                   0,
                                   self.right_world,
                                   self.right_world_turn_buffers.only_left,
                                   self.right_grid_left_exits,
                                   self.right_grid_right_exits,
                                   '12%+')
            es_max = np.unique(erS)[-2] + 1

        else:
            srS = self.get_statics(Direction.right,
                                   0,
                                   0,
                                   self.right_world,
                                   self.right_world_turn_buffers.only_right,
                                   self.right_grid_left_exits,
                                   self.right_grid_right_exits,
                                   '12%*')
            srs_max = np.unique(srS)[-2] + 1
            erS = self.get_statics(Direction.right,
                                   0,
                                   0,
                                   self.right_world,
                                   self.right_world_turn_buffers.only_right,
                                   self.right_grid_left_exits,
                                   self.right_grid_right_exits,
                                   '12%+')
            ers_max = np.unique(erS)[-2] + 1
            smS = self.get_statics(Direction.right,
                                   0,
                                   srs_max,
                                   self.main_world,
                                   self.main_world_turn_buffers.only_right,
                                   self.main_grid_left_exits,
                                   self.main_grid_right_exits,
                                   '12%*')
            sms_max = np.unique(smS)[-2] + 1
            emS = self.get_statics(Direction.right,
                                   0,
                                   ers_max,
                                   self.main_world,
                                   self.main_world_turn_buffers.only_right,
                                   self.main_grid_left_exits,
                                   self.main_grid_right_exits,
                                   '12%+')
            ems_max = np.unique(emS)[-2] + 1
            slS = self.get_statics(Direction.right,
                                   0,
                                   sms_max,
                                   self.left_world,
                                   self.left_world_turn_buffers.only_right,
                                   self.left_grid_left_exits,
                                   self.left_grid_right_exits,
                                   '12%*')
            ss_max = np.unique(slS)[-2] + 1
            elS = self.get_statics(Direction.right,
                                   0,
                                   ems_max,
                                   self.left_world,
                                   self.left_world_turn_buffers.only_right,
                                   self.left_grid_left_exits,
                                   self.left_grid_right_exits,
                                   '12%+')
            es_max = np.unique(elS)[-2] + 1

        inf = 1000
        slS[slS < inf] = ss_max - slS[slS < inf]
        slS[slS == inf] = -inf
        srS[srS < inf] = ss_max - srS[srS < inf]
        srS[srS == inf] = -inf
        smS[smS < inf] = ss_max - smS[smS < inf]
        smS[smS == inf] = -inf

        elS[elS < inf] = es_max - elS[elS < inf]
        elS[elS == inf] = -inf
        erS[erS < inf] = es_max - erS[erS < inf]
        erS[erS == inf] = -inf
        emS[emS < inf] = es_max - emS[emS < inf]
        emS[emS == inf] = -inf

        return slS, elS, srS, erS, smS, emS

    def get_statics(self, direction, left_smax, right_smax, world, world_turn_buffers, left_exits, right_exits,
                    forbidden):
        inf = 1000
        s = np.full_like(world, inf, dtype=float)
        visited = np.full_like(world, False, dtype=bool)

        left_old_visits = []
        if direction == Direction.both or direction == Direction.left:
            left_old_visits = self.fill_for_direction_before_end(Direction.left,
                                                                 s,
                                                                 left_exits,
                                                                 left_smax,
                                                                 visited,
                                                                 world,
                                                                 world_turn_buffers,
                                                                 forbidden)

        right_old_visits = []
        if direction == Direction.both or direction == Direction.right:
            right_old_visits = self.fill_for_direction_before_end(Direction.right,
                                                                  s,
                                                                  right_exits,
                                                                  right_smax,
                                                                  visited,
                                                                  world,
                                                                  world_turn_buffers,
                                                                  forbidden)

        if len(left_old_visits) > 0 and len(right_old_visits) > 0:
            old_visits = np.append(left_old_visits, right_old_visits, 0)
        elif len(left_old_visits) > 0:
            old_visits = left_old_visits
        elif len(right_old_visits) > 0:
            old_visits = right_old_visits
        else:
            old_visits = []

        if len(old_visits) > 0:
            self.fill_to_end(old_visits, visited, forbidden, s, world)

        return s

    def fill_for_direction_before_end(self, direction, s, exits, smax, visited, world, world_turn_buffers, forbidden):
        for pos in exits:
            s[pos] = smax
            visited[pos] = True

        old_visits = np.array(exits)
        for tb in world_turn_buffers:
            if (tb.direction == direction
                    and (tb.access_type == TurnBufferAccessType.normal
                         or (tb.access_type == TurnBufferAccessType.escalators and self.estairs in forbidden)
                         or (tb.access_type == TurnBufferAccessType.stairs and self.escalators in forbidden))):

                if tb.turn is None:
                    torb = tb.buffer
                else:
                    torb = tb.turn
                smax = self.go_to_reach_torb(torb, smax, old_visits, visited, forbidden, s, world)

                turn_old_visits = []
                if tb.turn is not None:
                    # Fill the turn area
                    smax = self.fill_torb(TorbType.turn, tb.type, tb.turn, smax, visited, forbidden, s, world)
                    turn_old_visits = self.fill_torb_side(tb.type, tb.turn, smax, visited, forbidden, s, world)

                buffer_old_visits = []
                if tb.buffer is not None:
                    # Fill the buffer area
                    smax = self.fill_torb(TorbType.buffer, tb.type, tb.buffer, smax, visited, forbidden, s, world)
                    buffer_old_visits = self.fill_torb_side(tb.type, tb.buffer, smax, visited, forbidden, s, world)

                if len(turn_old_visits) > 0 and len(buffer_old_visits) > 0:
                    old_visits = np.append(turn_old_visits, buffer_old_visits, 0)
                elif len(turn_old_visits) > 0:
                    old_visits = turn_old_visits
                elif len(buffer_old_visits) > 0:
                    old_visits = buffer_old_visits
                else:
                    old_visits = []

        return old_visits

    def fill_to_end(self, old_visits, visited, forbidden, s, world):
        new_visits = True
        while new_visits:
            new_visits = []
            for i1, j1 in old_visits:
                if world[i1, j1] not in forbidden:
                    for i2, j2 in self.get_neighbours(np.array([i1, j1]), s.shape):
                        if not visited[i2, j2] and world[i2, j2] not in forbidden:
                            new_visits.append((i2, j2))
                            visited[i2, j2] = True
                            s[i2, j2] = s[i1, j1] + 1
            old_visits = np.array(new_visits)

    def go_to_reach_torb(self, torb, smax, old_visits, visited, forbidden, s, world):
        new_visits = True
        while new_visits:
            new_visits = []
            for i1, j1 in old_visits:
                for i2, j2 in self.get_neighbours(np.array([i1, j1]), s.shape):
                    if (not visited[i2, j2]
                            and world[i2, j2] not in forbidden
                            and not ((torb[0] <= i2 <= torb[2])
                                     and (torb[1] <= j2 <= torb[3]))):
                        new_visits.append((i2, j2))
                        visited[i2, j2] = True
                        s[i2, j2] = s[i1, j1] + 1
                        smax = max(s[i2, j2], smax)

            old_visits = np.array(new_visits)
        smax += 1

        return smax

    def fill_torb(self, torb_type, tb_type, torb, smax, visited, forbidden, s, world):
        inf = 1000
        smax += 1
        if tb_type == TurnBufferType.double:
            if torb[6] % 2 == 0:
                torb_0 = torb[:2] + [(torb[0] + torb[1])//2, torb[3]] + torb[4:5] + torb[6:7]
                torb_1 = [(torb[0] + torb[1])//2 + 1, torb[1]] + torb[2:4] + torb[5:7]
            else:
                torb_0 = torb[:2] + [torb[2], (torb[1] + torb[3])//2] + torb[4:5] + torb[6:7]
                torb_1 = [torb[0], (torb[1] + torb[3])//2 + 1] + torb[2:4] + torb[5:7]
            torbs = [torb_0, torb_1]
        else:
            torbs = [torb]

        smaxs = []
        for trb in torbs:
            if torb_type == TorbType.turn:
                torb_s = self.get_turn_statics(world, trb, forbidden)
            else:
                torb_s = self.get_buffer_statics(world, trb, forbidden)

            torb_s[torb_s < inf] = torb_s[torb_s < inf] + smax
            s[trb[0]:trb[2] + 1, trb[1]:trb[3] + 1] = torb_s
            visited[trb[0]:trb[2] + 1, trb[1]:trb[3] + 1] = True
            smaxs.append(np.unique(torb_s)[-2] + 1)

        return max(smaxs)

    def fill_torb_side(self, tb_type, torb, smax, visited, forbidden, s, world):
        old_visits = []
        if tb_type == TurnBufferType.double:
            sides = torb[7:]
        else:
            sides = torb[6:]
        for side in sides:
            if side == 1:
                temp_old_visits = np.indices((1, torb[3] - torb[1] + 1)).reshape(2, -1).T
                temp_old_visits += np.array([torb[2] + 1, torb[1]])
                if old_visits:
                    old_visits = np.append(old_visits, temp_old_visits)
                else:
                    old_visits = temp_old_visits
                visited[torb[2] + 1:torb[2] + 2, torb[1]: torb[3] + 1] = True

            elif side == 2:
                temp_old_visits = np.indices((torb[2] - torb[0] + 1, 1)).reshape(2, -1).T
                temp_old_visits += np.array([torb[0], torb[3] + 1])
                if old_visits:
                    old_visits = np.append(old_visits, temp_old_visits)
                else:
                    old_visits = temp_old_visits
                visited[torb[0]: torb[2] + 1, torb[3] + 1: torb[3] + 2] = True

            elif side == 3:
                temp_old_visits = np.indices((1, torb[3] - torb[1] + 1)).reshape(2, -1).T
                temp_old_visits += np.array([torb[0] - 1, torb[1]])
                if old_visits:
                    old_visits = np.append(old_visits, temp_old_visits)
                else:
                    old_visits = temp_old_visits
                visited[torb[0] - 1: torb[0], torb[1]: torb[3] + 1] = True

            elif side == 4:
                temp_old_visits = np.indices((torb[2] - torb[0] + 1, 1)).reshape(2, -1).T
                temp_old_visits += np.array([torb[0], torb[1] - 1])
                if old_visits:
                    old_visits = np.append(old_visits, temp_old_visits)
                else:
                    old_visits = temp_old_visits
                visited[torb[0]: torb[2] + 1, torb[1] - 1: torb[1]] = True

        if len(old_visits) > 0:
            temp_old_visits = []
            for i1, j1 in old_visits:
                if world[i1, j1] not in forbidden:
                    s[i1, j1] = smax
                    temp_old_visits.append((i1, j1))
            old_visits = np.array(temp_old_visits)

        return old_visits

    def get_turn_statics(self, world, turn, forbidden):
        inf = 1000
        shape = turn[2] - turn[0] + 1, turn[3] - turn[1] + 1
        s = np.zeros(shape)
        n = 2 * max(shape)

        sl = np.zeros(2 * n + 1)
        for i in range(2 * n + 1):
            sl[i] = i * max(shape) / (2 * n)

        for i, j in np.indices(shape).reshape(2, -1).T:
            l = np.zeros(2 * n + 1)
            eta1 = np.arctan(shape[1] / shape[0]) / n
            eta2 = np.arctan(shape[0] / shape[1]) / n
            for k in range(n):
                l[k] = line_segment_length_in_grid_cell(i, j, np.tan(k * eta1))
            for k in range(n + 1):
                l[n + k] = line_segment_length_in_grid_cell(i, j, np.tan(k * eta2 + n * eta1))

            if turn[4] % 2 == 1:
                l = np.flip(l)

            s[i, j] = np.sum(sl * l) / np.sum(l)

        if turn[4] == 2 or turn[5] == 2:
            s = np.fliplr(s)

        if turn[4] == 1 or turn[5] == 1:
            s = np.flipud(s)

        for i, j in np.indices(shape).reshape(2, -1).T:
            if world[turn[0] + i, turn[1] + j] in forbidden:
                s[i, j] = inf

        return s

    def get_buffer_statics(self, world, buffer, forbidden):
        inf = 1000
        shape = buffer[2] - buffer[0] + 1, buffer[3] - buffer[1] + 1
        s = np.zeros(shape)
        d = 0.5
        theta = np.pi / 3

        if buffer[4] % 2 == 0:
            theta = np.pi / 2 - theta

        n = int((np.tan(theta) * shape[0] + shape[1]) / d)

        sl = np.zeros(n + 1)
        for i in range(n + 1):
            sl[i] = i * min(shape) / n

        for i, j in np.indices(shape).reshape(2, -1).T:
            l = np.zeros(n + 1)

            for k in range(n + 1):
                l[k] = line_segment_length_in_grid_cell(i, j, np.tan(theta), shape[1] - (k + 1) * d)

            s[i, j] = np.sum(sl * l) / np.sum(l)

        if buffer[4] == 1 or buffer[5] == 3:
            s = np.flipud(s)

        if buffer[5] == 2 or buffer[4] == 4:
            s = np.fliplr(s)

        for i, j in np.indices(shape).reshape(2, -1).T:
            if world[buffer[0] + i, buffer[1] + j] in forbidden:
                s[i, j] = inf

        return s

    def get_d(self, world, walls):
        inf = 1000
        d = np.full_like(world, inf, dtype=int)
        visited = np.full_like(world, False, dtype=bool)
        for pos in walls:
            d[pos] = 0
            visited[pos] = True

        old_visits = np.array(walls)
        new_visits = True

        while new_visits:
            new_visits = []
            for i1, j1 in old_visits:
                for i2, j2 in self.get_neighbours(np.array([i1, j1]), d.shape):
                    if not visited[i2, j2]:
                        new_visits.append((i2, j2))
                        visited[i2, j2] = True
                        d[i2, j2] = d[i1, j1] + 1
            old_visits = np.array(new_visits)

        return d

    def get_lneighbours(self, tunnel_name, p):
        if tunnel_name == TunnelName.main:
            width = self.left_width
        # Right
        else:
            width = self.main_width

        loffsets = np.array([[width - 1, 0], [width - 1, 1], [width - 1, -1]])
        offsets = np.array([[0, -1], [1, 0], [0, 1], [1, -1], [1, 1]])

        return p + loffsets, p + offsets

    def get_rneighbours(self, tunnel_name, p):
        if tunnel_name == TunnelName.main:
            width = self.main_width
        # Left
        else:
            width = self.left_width

        roffsets = np.array([[-width + 1, 0], [-width + 1, 1], [-width + 1, -1]])
        offsets = np.array([[-1, 0], [0, -1], [0, 1], [-1, 1], [-1, -1]])

        return p + roffsets, p + offsets

    def get_neighbours(self, p, shape):
        offsets = np.array([[-1, 0], [0, -1], [1, 0], [0, 1], [-1, 1], [-1, -1], [1, -1], [1, 1]])

        neighbours = p + offsets  # apply offsets to p

        # Exclude out-of-bounds indices
        valid = np.all((neighbours < np.array(shape)) & (neighbours >= 0), axis=1)
        neighbours = neighbours[valid]

        return neighbours

    def divide_initial_passengers_number(self, n):
        m = len(self.main_grid_empties)
        l = len(self.left_grid_empties)
        r = len(self.right_grid_empties)
        s = m + l + r
        mn = round((m * n) / s)
        ln = round((l * (n - mn)) / s)
        rn = n - mn - ln
        return mn, ln, rn
