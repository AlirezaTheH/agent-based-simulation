import tornado.web
import numpy as np
from mesa.visualization.ModularVisualization import ModularServer, PageHandler, SocketHandler
from mesa.visualization.UserParam import UserSettableParameter

from visualization import *
from simulator import Simulator
from dtypes import (DoorScenario,
                    TunnelName,
                    Direction,
                    TurnBufferType,
                    TurnBufferAccessType,
                    turn_buffer,
                    world_turn_buffers)


class Server(ModularServer):
    page_handler = (r"/", PageHandler)
    socket_handler = (r"/ws", SocketHandler)
    static_handler = (
        r"/static/(.*)",
        tornado.web.StaticFileHandler,
        {"path": "templates"},
    )
    local_handler = (r"/local/(.*)", tornado.web.StaticFileHandler, {"path": ""})
    handlers = [page_handler, socket_handler, static_handler, local_handler]
    settings = {
        "debug": True,
        "autoreload": False,
        "template_path": "templates",
    }


def extract_turn_buffer(line):
    if line[1] == 'l':
        direction = Direction.left
    else:
        direction = Direction.right

    if line[2] == 'e':
        access_type = TurnBufferAccessType.escalators
    elif line[2] == 's':
        access_type = TurnBufferAccessType.stairs
    else:
        access_type = TurnBufferAccessType.normal

    if line[3] == 's':
        tb_type = TurnBufferType.single
    else:
        tb_type = TurnBufferType.double

    turn, buffer = line[4:].strip().split('b')
    turn = list(map(int, turn.strip().split(' ')))
    if turn[0] == -1:
        turn = None
    buffer = list(map(int, buffer.strip().split(' ')))
    if buffer[0] == -1:
        buffer = None
    return turn_buffer(turn, buffer, tb_type, access_type, direction)


def read_tunnel_file(filepath):
    with open(filepath) as f:
        world = []
        both_turn_buffers = []
        only_left_turn_buffers = []
        only_right_turn_buffers = []
        line = f.readline()

        # Read the map
        while not line.startswith('b:'):
            world.append(line.strip().split())
            line = f.readline()

        # Read turn buffers for when both doors are open
        line = f.readline()
        while not line.startswith('l:'):
            both_turn_buffers.append(extract_turn_buffer(line))
            line = f.readline()

        # Read turn buffers for when only left door is open
        line = f.readline()
        while not line.startswith('r:'):
            only_left_turn_buffers.append(extract_turn_buffer(line))
            line = f.readline()

        # Read turn buffers for when only right door is open
        line = f.readline()
        while line:
            only_right_turn_buffers.append(extract_turn_buffer(line))
            line = f.readline()

        world.reverse()
        world = np.array(world).T

        return world, world_turn_buffers(both_turn_buffers, only_left_turn_buffers, only_right_turn_buffers)


main_world, main_world_turn_buffers = read_tunnel_file('inputs/main tunnel-temp.txt')
width, height = main_world.shape
main_canvas = MainCanvas(draw, width, height, width * 20, height * 20, TunnelName.main)

left_world, left_world_turn_buffers = read_tunnel_file('inputs/left tunnel-temp.txt')
width, height = left_world.shape
left_canvas = LeftCanvas(draw, width, height, width * 20, height * 20, TunnelName.left)

right_world, right_world_turn_buffers = read_tunnel_file('inputs/right tunnel-temp.txt')
width, height = right_world.shape
right_canvas = RightCanvas(draw, width, height, width * 20, height * 20, TunnelName.right)


time_element = TimeElement()
main_tunnel_passengers_element = MainTunnelPassengersElement()
left_tunnel_passengers_element = LeftTunnelPassengersElement()
right_tunnel_passengers_element = RightTunnelPassengersElement()
model_params = {
    'main_world': main_world,
    'left_world': left_world,
    'right_world': right_world,
    'main_world_turn_buffers': main_world_turn_buffers,
    'left_world_turn_buffers': left_world_turn_buffers,
    'right_world_turn_buffers': right_world_turn_buffers,
    'door_scenario': UserSettableParameter('choice',
                                           'Doors Scenario',
                                           DoorScenario.both,
                                           choices=[DoorScenario.both,
                                                    DoorScenario.only_left,
                                                    DoorScenario.only_right]),
    'ks': UserSettableParameter('number',
                                'ks',
                                1),
    'kd': UserSettableParameter('number',
                                'kd',
                                0.2),
    'delta': UserSettableParameter('number',
                                   'δ',
                                   0.2),
    'alpha': UserSettableParameter('number',
                                   'α',
                                   0.2),
    'grid_scale': UserSettableParameter('number',
                                        'Grid Scale (m)',
                                        0.4),
    'city_passengers_min_wall_distance': UserSettableParameter('number',
                                                               'City Passengers min Wall Distance (m)',
                                                               0.43),
    'intercity_passengers_min_wall_distance': UserSettableParameter('number',
                                                                    'Intercity Passengers min Wall Distance (m)',
                                                                    0.65),
    'city_passengers_velocity': UserSettableParameter('number',
                                                      'City Passengers Velocity (m/s)',
                                                      1.33),
    'intercity_passengers_velocity': UserSettableParameter('number',
                                                           'Intercity Passengers Velocity (m/s)',
                                                           1.14),
    'stairs_velocity': UserSettableParameter('number',
                                             'Passengers Velocity on Stairs (m/s)',
                                             0.7),
    'escalators_velocity': UserSettableParameter('number',
                                                 'Passengers Velocity on Escalators (m/s)',
                                                 0.75),
    'city_passengers_escalator_usage_percent': UserSettableParameter('number',
                                                                     'City Passengers Escalator Usage Percent',
                                                                     60),
    'intercity_passengers_escalator_usage_percent': UserSettableParameter(
        'number',
        'Intercity Passengers Escalator Usage Percent',
        60),
    'initial_city_passenger_number': UserSettableParameter('number',
                                                           'Number of Initial City Passengers',
                                                           100),
    'initial_intercity_passenger_number': UserSettableParameter('number',
                                                                'Number of Initial Intercity Passengers',
                                                                100),
    'ea_city_passenger_number': UserSettableParameter('number',
                                                      'Number of City Passengers Come from Entrance A',
                                                      296),
    'ea_intercity_passenger_number': UserSettableParameter('number',
                                                           'Number of Intercity Passengers Come from Entrance A',
                                                           104),
    'eb_city_passenger_number': UserSettableParameter('number',
                                                      'Number of City Passengers Come from Entrance B',
                                                      84),
    'eb_intercity_passenger_number': UserSettableParameter('number',
                                                           'Number of Intercity Passengers Come from Entrance B',
                                                           336),
    'ec_city_passenger_number': UserSettableParameter('number',
                                                      'Number of City Passengers Come from Entrance C',
                                                      0),
    'ec_intercity_passenger_number': UserSettableParameter('number',
                                                           'Number of Intercity Passengers Come from Entrance C',
                                                           0),
    'drop_baggage_in_congestion_percent': UserSettableParameter('number',
                                                                'Drop Baggage in Congestion Percent',
                                                                0)
}

server = Server(
    Simulator, [time_element,
                main_tunnel_passengers_element,
                left_tunnel_passengers_element,
                right_tunnel_passengers_element,
                left_canvas, main_canvas, right_canvas], 'Simulator', model_params
)
