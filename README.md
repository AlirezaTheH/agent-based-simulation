# Agent Based Simulation

An agent based simulation for arrivals and departures of passengers in a tunnel.

## Installation

To install the simulator enter following commands in terminal or windows git bash:
```bash
git clone https://gitlab.com/AlirezaH320/agent-based-simulation.git
cd agent-based-simulation
python -m venv venv
```
in windows gitbash:
```bash
source ./venv/Scripts/activate
pip install -r requirements.txt
```
in terminal:
```bash
source venv/bin/activate
pip install -r requirements.txt
```

## Update
To update project enter following commands in terminal or windows git bash:
```bash
cd agent-based-simulation
source ./venv/Scripts/activate
pip install -r requirements.txt --upgrade
git pull
```

## Run
In the project directory enter the following commands:

in windows git bash:
```bash
source ./venv/Scripts/activate
python run.py
```

in terminal:
```bash
source venv/bin/activate
python run.py
```

## Map definitions
Every cell of a map is defined by one of the following characters:

| char | definition                         |
|------|------------------------------------|
| %    | not defined                        |
| 0    | empty                              |
| 1    | wall                               |
| 2    | obstacle                           | 
| 4    | indoors                            |
| 5    | indoors                            |
| 6    | indoors                            |
| l    | tunnel left outdoors               |
| r    | tunnel right outdoors              |
| =    | forced stairs                      |
| +    | choosable stairs                   |
| *    | escalators                         |
| e    | pre and post stairs and escalators | 

At the end of the map turn and buffer blocks can be defined. 
for every door scenario, turn and buffer block must be defined separately. 
every scenario starts with one of the following lines.

| door scenario           | starting line |
|-------------------------|---------------|
| both doors is open      | b:            |
| only left door is open  | l:            |
| only right door is open | r:            |

then then every turn and buffer can be defined with a line
starting with a four letter word (we call it **tdat**) 
following with at least 6 numbers or a -1 if it does not contain turn block and continued
with a letter **b** 
following with at least 6 numbers or a -1 if it does not contain a buffer block.
the first 4 numbers are coordinates, 5th (and 6th in doubles) is turn out direction
and the 6th (or 7th in doubles) is turn in direction, 
and next numbers exist are turn or buffer sides that must be filled, with following definitions:

| number | definition |
|--------|------------|
| 1      | right      |
| 2      | up         |
| 3      | left       |
| 4      | down       |

#### tdat:
tdat explains turn's direction and access type and type, first character is always **t**,
and for the other three we can use following tables:

| 2nd char | definition                                       |
|----------|--------------------------------------------------|
| l        | direction in defined door scenario is left       |
| r        | turn direction in defined door scenario is right |


| 3rd char | definition                                            |
|----------|-------------------------------------------------------|
| s        | will be used only for choosable stairs statics        |
| e        | will be used only for choosable escalators statics    |
| n        | will be used for both choosable stairs and escalators |


| 4th char | definition                                                                  |
|----------|-----------------------------------------------------------------------------|
| s        | it's a single turn buffer. single turn or buffer has only one out direction |
| d        | it's a double turn buffer. double turn or buffer has two out direction      |

#### examples:
```
l:
trns 0 7 19 26 1 2 b 0 27 19 30 1 2 2
tles -1 b 0 58 19 147 2 1 4
tred -1 b 231 7 241 26 4 2 3 3
```