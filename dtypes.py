from collections import namedtuple


class DoorScenario:
    both = 'Both are open'
    only_left = 'Only left is open'
    only_right = 'Only right is open'


class TunnelName:
    left = 'left'
    main = 'main'
    right = 'right'


class PassengerPathDesire:
    stairs = 'stairs'
    escalators = 'escalators'


class TurnBufferAccessType:
    normal = 'normal'
    stairs = 'stairs'
    escalators = 'escalators'


class TurnBufferType:
    double = 'double'
    single = 'single'


class Direction:
    both = 'both'
    left = 'left'
    right = 'right'


class TorbType:
    turn = 'turn'
    buffer = 'buffer'


turn_buffer = namedtuple('TurnBuffer', ['turn',
                                        'buffer',
                                        'type',
                                        'access_type',
                                        'direction'])

world_turn_buffers = namedtuple('WorldTurnBuffers', ['both',
                                                     'only_left',
                                                     'only_right'])
