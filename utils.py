import numpy as np


def line_segment_length_in_grid_cell(cellx, celly, m, b=0):
    intersection_points = set()
    y1 = m * cellx + b
    if celly <= y1 <= celly + 1:
        intersection_points.add((cellx, y1))

    y2 = m * (cellx + 1) + b
    if celly <= y2 <= celly + 1:
        intersection_points.add((cellx + 1, y2))

    if m != 0:
        x1 = (celly - b) / m
        if cellx <= x1 <= cellx + 1:
            intersection_points.add((x1, celly))

        x2 = (celly + 1 - b) / m
        if cellx <= x2 <= cellx + 1:
            intersection_points.add((x2, celly + 1))

    if len(intersection_points) == 2:
        a, b = intersection_points
        length = np.linalg.norm(np.array(a)-np.array(b))

    else:
        length = 0

    return length
