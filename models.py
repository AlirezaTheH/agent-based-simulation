import numpy as np
from mesa import Agent
import random

from dtypes import (DoorScenario,
                    TunnelName,
                    PassengerPathDesire)


class Wall(Agent):
    def __init__(self, pos, model, grid):
        super().__init__(pos, model)
        grid.place_agent(self, pos)


class Obstacle(Agent):
    def __init__(self, pos, model, grid):
        super().__init__(pos, model)
        grid.place_agent(self, pos)


class Baggage(Agent):
    def __init__(self, pos, model, tunnel_name):
        super().__init__(pos, model)
        self.tunnel_name = tunnel_name

        if self.tunnel_name == TunnelName.main:
            self.model.main_grid.place_agent(self, pos)
        elif self.tunnel_name == TunnelName.left:
            self.model.left_grid.place_agent(self, pos)
        else:
            self.model.right_grid.place_agent(self, pos)


class CityPassenger(Agent):
    def __init__(self, unique_id, pos, model, path_desire, tunnel_name, min_wall_distance):
        super().__init__(unique_id, model)
        self.model.unique_id += 1
        self.default_interval = self.model.city_passengers_interval
        self.interval = self.default_interval
        self.tunnel_name = tunnel_name
        self.min_wall_distance = min_wall_distance
        self.path_desire = path_desire

        if self.tunnel_name == TunnelName.main:
            self.model.main_world_passengers += 1
            self.model.main_grid.place_agent(self, pos)
            if self.path_desire == PassengerPathDesire.escalators:
                self.S = self.model.emS
            else:
                self.S = self.model.smS

        elif self.tunnel_name == TunnelName.left:
            self.model.left_world_passengers += 1
            self.model.left_grid.place_agent(self, pos)
            if self.path_desire == PassengerPathDesire.escalators:
                self.S = self.model.elS
            else:
                self.S = self.model.slS

        else:
            self.model.right_world_passengers += 1
            self.model.right_grid.place_agent(self, pos)
            if self.path_desire == PassengerPathDesire.escalators:
                self.S = self.model.erS
            else:
                self.S = self.model.srS

        self.model.schedule.add(self)

    @property
    def mP(self):
        if self.path_desire == PassengerPathDesire.escalators:
            return self.model.emP
        else:
            return self.model.smP

    @property
    def lP(self):
        if self.path_desire == PassengerPathDesire.escalators:
            return self.model.elP
        else:
            return self.model.slP

    @property
    def rP(self):
        if self.path_desire == PassengerPathDesire.escalators:
            return self.model.erP
        else:
            return self.model.srP

    def reset_s(self):
        if self.tunnel_name == TunnelName.main:
            if self.path_desire == PassengerPathDesire.escalators:
                self.S = self.model.emS
            else:
                self.S = self.model.smS

        elif self.tunnel_name == TunnelName.left:
            if self.path_desire == PassengerPathDesire.escalators:
                self.S = self.model.elS
            else:
                self.S = self.model.slS

        else:
            if self.path_desire == PassengerPathDesire.escalators:
                self.S = self.model.erS
            else:
                self.S = self.model.srS

    def p(self, world, grid, d):
        p = []
        for pos in self.model.get_neighbours(np.array(self.pos), world.shape):
            pos = tuple(pos)
            if grid.is_cell_empty(pos) and (world[pos] in self.model.free_distance
                                            or d[pos] > self.min_wall_distance):
                p.append(pos)
        return p

    def sp(self, grid, sgrid, get_sneighbours, d, sd):
        sp = []
        p = []
        sneighbours, neighbours = get_sneighbours(self.tunnel_name, np.array(self.pos))

        for pos in sneighbours:
            pos = tuple(pos)
            if sgrid.is_cell_empty(pos) and sd[pos] > self.min_wall_distance:
                sp.append(pos)

        for pos in neighbours:
            pos = tuple(pos)
            if grid.is_cell_empty(pos) and d[pos] > self.min_wall_distance:
                p.append(pos)

        return sp, p

    def update_interval(self, world):
        if world[self.pos] == self.model.stairs:
            self.interval = self.model.stairs_interval
        elif world[self.pos] == self.model.escalators:
            self.interval = self.model.escalators_interval
        else:
            self.interval = self.default_interval

    def step(self):
        # Check velocity intervals
        if self.model.schedule.steps % self.interval == 0:

            # If agent is in main tunnel
            if self.tunnel_name == TunnelName.main:

                # If agent is in left edge of main tunnel
                if (self.model.main_world[self.pos] == self.model.left_exits
                        and self.model.door_scenario != DoorScenario.only_right):

                    # Filter free neighbours
                    lp, p = self.sp(self.model.main_grid,
                                    self.model.left_grid,
                                    self.model.get_lneighbours,
                                    self.model.md,
                                    self.model.ld)
                    if lp:
                        new_pos = max(lp, key=lambda k: self.lP[k])
                        if self.lP[new_pos] > self.mP[self.pos]:
                            self.model.main_grid.remove_agent(self)
                            self.model.left_grid.place_agent(self, new_pos)
                            self.tunnel_name = TunnelName.left
                            self.reset_s()
                            self.model.lD[new_pos] += 1
                            self.model.main_world_passengers -= 1
                            self.model.left_world_passengers += 1
                    elif p:
                        new_pos = max(p, key=lambda k: self.mP[k])
                        if self.mP[new_pos] > self.mP[self.pos]:
                            self.model.main_grid.move_agent(self, new_pos)
                            self.model.mD[new_pos] += 1

                # If agent is in right edge of main tunnel
                elif (self.model.main_world[self.pos] == self.model.right_exits
                      and self.model.door_scenario != DoorScenario.only_left):

                    # Filter free neighbours
                    rp, p = self.sp(self.model.main_grid,
                                    self.model.right_grid,
                                    self.model.get_rneighbours,
                                    self.model.md,
                                    self.model.rd)
                    if rp:
                        new_pos = max(rp, key=lambda k: self.rP[k])
                        if self.rP[new_pos] > self.mP[self.pos]:
                            self.model.main_grid.remove_agent(self)
                            self.model.right_grid.place_agent(self, new_pos)
                            self.tunnel_name = TunnelName.right
                            self.reset_s()
                            self.model.rD[new_pos] += 1
                            self.model.main_world_passengers -= 1
                            self.model.right_world_passengers += 1
                    elif p:
                        new_pos = max(p, key=lambda k: self.mP[k])
                        if self.mP[new_pos] > self.mP[self.pos]:
                            self.model.main_grid.move_agent(self, new_pos)
                            self.model.mD[new_pos] += 1

                # If agent is in somewhere in middle of main tunnel
                else:
                    # Filter free neighbours
                    p = self.p(self.model.main_world, self.model.main_grid, self.model.md)
                    if p:
                        new_pos = max(p, key=lambda k: self.mP[k])
                        if self.mP[new_pos] > self.mP[self.pos]:
                            self.model.main_grid.move_agent(self, new_pos)
                            self.model.mD[new_pos] += 1

            # If agent is in left tunnel
            elif self.tunnel_name == TunnelName.left:

                # If agent can exit
                if (self.model.left_world[self.pos] == self.model.left_exits
                        and self.model.door_scenario != DoorScenario.only_right):
                    self.model.schedule.remove(self)
                    self.model.left_grid.remove_agent(self)
                    self.model.left_world_passengers -= 1

                # If agent is in right edge of left tunnel
                elif (self.model.left_world[self.pos] == self.model.right_exits
                      and self.model.door_scenario == DoorScenario.only_right):

                    # Filter free neighbours
                    rp, p = self.sp(self.model.left_grid,
                                    self.model.main_grid,
                                    self.model.get_rneighbours,
                                    self.model.ld,
                                    self.model.md)
                    if rp:
                        new_pos = max(rp, key=lambda k: self.mP[k])
                        if self.mP[new_pos] > self.lP[self.pos]:
                            self.model.left_grid.remove_agent(self)
                            self.model.main_grid.place_agent(self, new_pos)
                            self.tunnel_name = TunnelName.main
                            self.reset_s()
                            self.model.mD[new_pos] += 1
                            self.model.left_world_passengers -= 1
                            self.model.main_world_passengers += 1
                    elif p:
                        new_pos = max(p, key=lambda k: self.lP[k])
                        if self.lP[new_pos] > self.lP[self.pos]:
                            self.model.left_grid.move_agent(self, new_pos)
                            self.model.lD[new_pos] += 1

                # If agent is in somewhere in middle of left tunnel
                else:
                    # Filter free neighbours
                    p = self.p(self.model.left_world, self.model.left_grid, self.model.ld)
                    if p:
                        new_pos = max(p, key=lambda k: self.lP[k])
                        if self.lP[new_pos] > self.lP[self.pos]:
                            self.model.left_grid.move_agent(self, new_pos)
                            self.model.lD[new_pos] += 1
                            self.update_interval(self.model.left_world)

            # If agent is in right tunnel
            else:

                # If agent can exit
                if (self.model.right_world[self.pos] == self.model.right_exits
                        and self.model.door_scenario != DoorScenario.only_left):
                    self.model.schedule.remove(self)
                    self.model.right_grid.remove_agent(self)
                    self.model.right_world_passengers -= 1

                # If agent is in left edge of right tunnel
                elif (self.model.right_world[self.pos] == self.model.left_exits
                      and self.model.door_scenario == DoorScenario.only_left):

                    # Filter free neighbours
                    lp, p = self.sp(self.model.right_grid,
                                    self.model.main_grid,
                                    self.model.get_lneighbours,
                                    self.model.rd,
                                    self.model.md)
                    if lp:
                        new_pos = max(lp, key=lambda k: self.mP[k])
                        if self.mP[new_pos] > self.rP[self.pos]:
                            self.model.right_grid.remove_agent(self)
                            self.model.main_grid.place_agent(self, new_pos)
                            self.tunnel_name = TunnelName.main
                            self.reset_s()
                            self.model.mD[new_pos] += 1
                            self.model.right_world_passengers -= 1
                            self.model.main_world_passengers += 1
                    elif p:
                        new_pos = max(p, key=lambda k: self.rP[k])
                        if self.rP[new_pos] > self.rP[self.pos]:
                            self.model.right_grid.move_agent(self, new_pos)
                            self.model.rD[new_pos] += 1

                # If agent is in somewhere in middle of right tunnel
                else:
                    # Filter free neighbours
                    p = self.p(self.model.right_world, self.model.right_grid, self.model.rd)
                    if p:
                        new_pos = max(p, key=lambda k: self.rP[k])
                        if self.rP[new_pos] > self.rP[self.pos]:
                            self.model.right_grid.move_agent(self, new_pos)
                            self.model.rD[new_pos] += 1
                            self.update_interval(self.model.right_world)


class IntercityPassenger(CityPassenger):
    def __init__(self, unique_id, pos, baggage, model, path_desire, tunnel_name, min_wall_distance):
        super().__init__(unique_id, pos, model, path_desire, tunnel_name, min_wall_distance)
        self.default_interval = self.model.intercity_passengers_interval
        self.interval = self.default_interval
        self.baggage = baggage
        self.has_baggage = True

    def in_congestion(self):
        if self.tunnel_name == TunnelName.main:
            if self.pos[0] == 0 or self.pos[0] == self.model.main_width - 1:
                return False
            neighbours = self.model.get_neighbours(np.array(self.pos), self.model.main_world.shape)
            for pos in neighbours:
                pos = tuple(pos)
                if self.model.main_grid.is_cell_empty(pos):
                    return False

        elif self.tunnel_name == TunnelName.left:
            if self.pos[0] == self.model.left_width - 1:
                return False
            neighbours = self.model.get_neighbours(np.array(self.pos), self.model.left_world.shape)
            for pos in neighbours:
                pos = tuple(pos)
                if self.model.left_grid.is_cell_empty(pos):
                    return False

        else:
            if self.pos[0] == 0:
                return False
            neighbours = self.model.get_neighbours(np.array(self.pos), self.model.right_world.shape)
            for pos in neighbours:
                pos = tuple(pos)
                if self.model.right_grid.is_cell_empty(pos):
                    return False

        return True

    def step(self):
        # Check velocity intervals
        if self.model.schedule.steps % self.interval == 0:

            # If agent is in congestion and may drop its baggage
            if self.has_baggage:
                drop_case = random.choice(self.model.drop_baggage_or_not)
                if drop_case and self.in_congestion():
                    self.has_baggage = False

            # If agent is in main tunnel
            if self.tunnel_name == TunnelName.main:

                # If agent is in left edge of main tunnel
                if (self.model.main_world[self.pos] == self.model.left_exits
                        and self.model.door_scenario != DoorScenario.only_right):

                    # Filter free neighbours
                    lp, p = self.sp(self.model.main_grid,
                                    self.model.left_grid,
                                    self.model.get_lneighbours,
                                    self.model.md,
                                    self.model.ld)
                    if lp:
                        new_pos = max(lp, key=lambda k: self.lP[k])
                        if self.lP[new_pos] > self.mP[self.pos]:
                            pos = self.pos
                            self.model.main_grid.remove_agent(self)
                            self.model.left_grid.place_agent(self, new_pos)
                            if self.has_baggage:
                                self.model.main_grid.move_agent(self.baggage, pos)
                            self.tunnel_name = TunnelName.left
                            self.reset_s()
                            self.model.lD[new_pos] += 1
                            self.model.main_world_passengers -= 1
                            self.model.left_world_passengers += 1
                    elif p:
                        new_pos = max(p, key=lambda k: self.mP[k])
                        if self.mP[new_pos] > self.mP[self.pos]:
                            pos = self.pos
                            self.model.main_grid.move_agent(self, new_pos)
                            if self.has_baggage:
                                self.model.main_grid.move_agent(self.baggage, pos)
                            self.model.mD[new_pos] += 1

                # If agent is in right edge of main tunnel
                elif (self.model.main_world[self.pos] == self.model.right_exits
                      and self.model.door_scenario != DoorScenario.only_left):

                    # Filter free neighbours
                    rp, p = self.sp(self.model.main_grid,
                                    self.model.right_grid,
                                    self.model.get_rneighbours,
                                    self.model.md,
                                    self.model.rd)
                    if rp:
                        new_pos = max(rp, key=lambda k: self.rP[k])
                        if self.rP[new_pos] > self.mP[self.pos]:
                            pos = self.pos
                            self.model.main_grid.remove_agent(self)
                            self.model.right_grid.place_agent(self, new_pos)
                            if self.has_baggage:
                                self.model.main_grid.move_agent(self.baggage, pos)
                            self.tunnel_name = TunnelName.right
                            self.reset_s()
                            self.model.rD[new_pos] += 1
                            self.model.main_world_passengers -= 1
                            self.model.right_world_passengers += 1
                    elif p:
                        new_pos = max(p, key=lambda k: self.mP[k])
                        if self.mP[new_pos] > self.mP[self.pos]:
                            pos = self.pos
                            self.model.main_grid.move_agent(self, new_pos)
                            if self.has_baggage:
                                self.model.main_grid.move_agent(self.baggage, pos)
                            self.model.mD[new_pos] += 1

                # If agent is in somewhere in middle of main tunnel
                else:
                    # Filter free neighbours
                    p = self.p(self.model.main_world, self.model.main_grid, self.model.md)
                    if p:
                        pos = self.pos
                        new_pos = max(p, key=lambda k: self.mP[k])
                        if self.mP[new_pos] > self.mP[self.pos]:
                            self.model.main_grid.move_agent(self, new_pos)
                            if self.has_baggage:

                                # If baggage is in same tunnel
                                if self.baggage.tunnel_name == TunnelName.main:
                                    self.model.main_grid.move_agent(self.baggage, pos)

                                # If baggage is in other tunnels
                                else:
                                    if self.baggage.tunnel_name == TunnelName.left:
                                        self.model.left_grid.remove_agent(self.baggage)
                                    else:
                                        self.model.right_grid.remove_agent(self.baggage)
                                    self.model.main_grid.place_agent(self.baggage, pos)
                                    self.baggage.tunnel_name = TunnelName.main

                            self.model.mD[new_pos] += 1

            # If agent is in left tunnel
            elif self.tunnel_name == TunnelName.left:

                # If agent can exit
                if (self.model.left_world[self.pos] == self.model.left_exits
                        and self.model.door_scenario != DoorScenario.only_right):
                    self.model.schedule.remove(self)
                    self.model.left_grid.remove_agent(self)
                    if self.has_baggage:
                        self.model.left_grid.remove_agent(self.baggage)
                    self.model.left_world_passengers -= 1

                # If agent is in right edge of left tunnel
                elif (self.model.left_world[self.pos] == self.model.right_exits
                      and self.model.door_scenario == DoorScenario.only_right):

                    # Filter free neighbours
                    rp, p = self.sp(self.model.left_grid,
                                    self.model.main_grid,
                                    self.model.get_rneighbours,
                                    self.model.ld,
                                    self.model.md)
                    if rp:
                        new_pos = max(rp, key=lambda k: self.mP[k])
                        if self.mP[new_pos] > self.lP[self.pos]:
                            pos = self.pos
                            self.model.left_grid.remove_agent(self)
                            self.model.main_grid.place_agent(self, new_pos)
                            if self.has_baggage:
                                self.model.left_grid.move_agent(self.baggage, pos)
                            self.tunnel_name = TunnelName.main
                            self.reset_s()
                            self.model.mD[new_pos] += 1
                            self.model.left_world_passengers -= 1
                            self.model.main_world_passengers += 1
                    elif p:
                        new_pos = max(p, key=lambda k: self.mP[k])
                        if self.mP[new_pos] > self.mP[self.pos]:
                            pos = self.pos
                            self.model.main_grid.move_agent(self, new_pos)
                            if self.has_baggage:
                                self.model.main_grid.move_agent(self.baggage, pos)
                            self.model.mD[new_pos] += 1

                # If agent is in somewhere middle of left tunnel
                else:
                    # Filter free neighbours
                    p = self.p(self.model.left_world, self.model.left_grid, self.model.ld)
                    if p:
                        new_pos = max(p, key=lambda k: self.lP[k])
                        if self.lP[new_pos] > self.lP[self.pos]:
                            pos = self.pos
                            self.model.left_grid.move_agent(self, new_pos)
                            if self.has_baggage:

                                # If baggage is in same tunnel
                                if self.baggage.tunnel_name == TunnelName.left:
                                    self.model.left_grid.move_agent(self.baggage, pos)

                                # If baggage is in main tunnel
                                else:
                                    self.model.main_grid.remove_agent(self.baggage)
                                    self.model.left_grid.place_agent(self.baggage, pos)
                                    self.baggage.tunnel_name = TunnelName.left
                            self.model.lD[new_pos] += 1
                            self.update_interval(self.model.left_world)

            # If agent is in right tunnel
            else:

                # If agent can exit
                if (self.model.right_world[self.pos] == self.model.right_exits
                        and self.model.door_scenario != DoorScenario.only_left):
                    self.model.schedule.remove(self)
                    self.model.right_grid.remove_agent(self)
                    if self.has_baggage:
                        self.model.right_grid.remove_agent(self.baggage)
                    self.model.right_world_passengers -= 1

                # If agent is in left edge of right tunnel
                elif (self.model.right_world[self.pos] == self.model.left_exits
                      and self.model.door_scenario == DoorScenario.only_left):
                    # Filter free neighbours
                    lp, p = self.sp(self.model.right_grid,
                                    self.model.main_grid,
                                    self.model.get_lneighbours,
                                    self.model.rd,
                                    self.model.md)
                    if lp:
                        new_pos = max(lp, key=lambda k: self.mP[k])
                        if self.mP[new_pos] > self.rP[self.pos]:
                            pos = self.pos
                            self.model.right_grid.remove_agent(self)
                            self.model.main_grid.place_agent(self, new_pos)
                            if self.has_baggage:
                                self.model.right_grid.move_agent(self.baggage, pos)
                            self.tunnel_name = TunnelName.main
                            self.reset_s()
                            self.model.mD[new_pos] += 1
                            self.model.right_world_passengers -= 1
                            self.model.main_world_passengers += 1
                    elif p:
                        new_pos = max(p, key=lambda k: self.rP[k])
                        if self.rP[new_pos] > self.rP[self.pos]:
                            pos = self.pos
                            self.model.right_grid.move_agent(self, new_pos)
                            if self.has_baggage:
                                self.model.right_grid.move_agent(self.baggage, pos)
                            self.model.rD[new_pos] += 1

                # If agent is in somewhere middle of right tunnel
                else:
                    # Filter free neighbours
                    p = self.p(self.model.right_world, self.model.right_grid, self.model.rd)
                    if p:
                        new_pos = max(p, key=lambda k: self.rP[k])
                        if self.rP[new_pos] > self.rP[self.pos]:
                            pos = self.pos
                            self.model.right_grid.move_agent(self, new_pos)
                            if self.has_baggage:

                                # If baggage is in same tunnel
                                if self.baggage.tunnel_name is TunnelName.right:
                                    self.model.right_grid.move_agent(self.baggage, pos)

                                # If baggage is in main tunnel
                                else:
                                    self.model.main_grid.remove_agent(self.baggage)
                                    self.model.right_grid.place_agent(self.baggage, pos)
                                    self.baggage.tunnel_name = TunnelName.right

                            self.model.rD[new_pos] += 1
                            self.update_interval(self.model.right_world)
