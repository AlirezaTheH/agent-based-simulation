from collections import defaultdict

from mesa.visualization.modules import CanvasGrid, TextElement

from models import (Wall,
                    Obstacle,
                    CityPassenger,
                    IntercityPassenger,
                    Baggage)


class TimeElement(TextElement):
    """
    Display a text count of how many happy agents there are.
    """

    def render(self, model):
        return 'Time (s): {}'.format(model.schedule.time / 10)


class MainTunnelPassengersElement(TextElement):
    def render(self, model):
        return 'Main Tunnel Remained Passengers: {}'.format(model.main_world_passengers)


class LeftTunnelPassengersElement(TextElement):
    def render(self, model):
        return 'Left Tunnel Remained Passengers: {}'.format(model.left_world_passengers)


class RightTunnelPassengersElement(TextElement):
    def render(self, model):
        return 'Right Tunnel Remained Passengers: {}'.format(model.right_world_passengers)


def draw(agent):
    """
    Portrayal method for canvas
    """
    if isinstance(agent, Wall):
        portrayal = {'Shape': 'rect',
                     'w': 1,
                     'h': 1,
                     'Filled': 'true',
                     'Layer': 1,
                     'Color': ['#9999ff', '#b3b3ff'],
                     'stroke_color': '#6666ff'}

    elif isinstance(agent, Obstacle):
        portrayal = {'Shape': 'rect',
                     'w': 1,
                     'h': 1,
                     'Filled': 'true',
                     'Layer': 1,
                     'Color': ['#66b3ff', '#80bfff'],
                     'stroke_color': '#99ccff'}

    elif isinstance(agent, IntercityPassenger):
        portrayal = {'Shape': 'circle',
                     'r': 0.9,
                     'Filled': 'true',
                     'Layer': 1,
                     'Color': ['#00804d', '#00995c'],
                     'stroke_color': '#00b36b'}

    elif isinstance(agent, CityPassenger):
        portrayal = {'Shape': 'circle',
                     'r': 0.9,
                     'Filled': 'true',
                     'Layer': 1,
                     'Color': ['#002266', '#002b80'],
                     'stroke_color': '#003399'}

    elif isinstance(agent, Baggage):
        portrayal = {'Shape': 'circle',
                     'r': 0.5,
                     'Filled': 'true',
                     'Layer': 1,
                     'Color': ['#00cc7a', '#00e68a'],
                     'stroke_color': '#00ff99'}
    else:
        portrayal = {'Shape': 'rect',
                     'w': 0,
                     'h': 0,
                     'Filled': 'true',
                     'Layer': 1,
                     'Color': ['#ffffff', '#ffffff'],
                     'stroke_color': '#6666ff'}

    return portrayal


class MainCanvas(CanvasGrid):
    def __init__(self, portrayal_method, grid_width, grid_height, canvas_width, canvas_height, tunnel_name):
        super().__init__(portrayal_method, grid_width, grid_height, canvas_width, canvas_height)
        self.set_js_code(tunnel_name)

    def set_js_code(self, tunnel_name):
        new_element = "new CanvasModule({}, {}, {}, {}, '{}')".format(
            self.canvas_width, self.canvas_height, self.grid_width, self.grid_height, tunnel_name
        )
        self.js_code = "elements.push(" + new_element + ");"

    def render(self, model):
        grid_state = defaultdict(list)
        for x in range(self.grid_width):
            for y in range(self.grid_height):
                obj = model.main_grid[x][y]
                portrayal = self.portrayal_method(obj)
                portrayal['x0'] = x
                portrayal['y0'] = y
                portrayal['x'] = x
                portrayal['y'] = y
                portrayal['eS'] = '-inf' if model.emS[x, y] == -1000 else '%.3f' % model.emS[x, y]
                portrayal['sS'] = '-inf' if model.smS[x, y] == -1000 else '%.3f' % model.smS[x, y]
                portrayal['D'] = '%.3f' % model.mD[x, y]
                grid_state[portrayal['Layer']].append(portrayal)
        return grid_state


class LeftCanvas(MainCanvas):
    def render(self, model):
        grid_state = defaultdict(list)
        for x in range(self.grid_width):
            for y in range(self.grid_height):
                obj = model.left_grid[x][y]
                portrayal = self.portrayal_method(obj)
                portrayal['x0'] = x
                portrayal['y0'] = y
                portrayal['x'] = x
                portrayal['y'] = y
                portrayal['eS'] = '-inf' if model.elS[x, y] == -1000 else '%.3f' % model.elS[x, y]
                portrayal['sS'] = '-inf' if model.slS[x, y] == -1000 else '%.3f' % model.slS[x, y]
                portrayal['D'] = '%.3f' % model.lD[x, y]
                grid_state[portrayal['Layer']].append(portrayal)

                if model.left_world[x, y] in model.stairs:
                    portrayal = {'Shape': 'rect',
                                 'x': x,
                                 'y': y,
                                 'w': 1,
                                 'h': 1,
                                 'Filled': 'true',
                                 'Layer': 0,
                                 'Color': ['#fff2e6', '#fff2e6'],
                                 'stroke_color': '#fff2e6'}
                    grid_state[portrayal['Layer']].append(portrayal)

                elif model.left_world[x, y] == model.escalators:
                    portrayal = {'Shape': 'rect',
                                 'x': x,
                                 'y': y,
                                 'w': 1,
                                 'h': 1,
                                 'Filled': 'true',
                                 'Layer': 0,
                                 'Color': ['#ffe6ff', '#ffe6ff'],
                                 'stroke_color': '#ffe6ff'}
                    grid_state[portrayal['Layer']].append(portrayal)

        return grid_state


class RightCanvas(MainCanvas):
    def render(self, model):
        grid_state = defaultdict(list)
        for x in range(self.grid_width):
            for y in range(self.grid_height):
                obj = model.right_grid[x][y]
                portrayal = self.portrayal_method(obj)
                portrayal['x0'] = x
                portrayal['y0'] = y
                portrayal['x'] = x
                portrayal['y'] = y
                portrayal['eS'] = '-inf' if model.erS[x, y] == -1000 else '%.3f' % model.erS[x, y]
                portrayal['sS'] = '-inf' if model.srS[x, y] == -1000 else '%.3f' % model.srS[x, y]
                portrayal['D'] = '%.3f' % model.rD[x, y]
                grid_state[portrayal['Layer']].append(portrayal)

                if model.right_world[x, y] in model.stairs:
                    portrayal = {'Shape': 'rect',
                                 'x': x,
                                 'y': y,
                                 'w': 1,
                                 'h': 1,
                                 'Filled': 'true',
                                 'Layer': 0,
                                 'Color': ['#fff2e6', '#fff2e6'],
                                 'stroke_color': '#fff2e6'}
                    grid_state[portrayal['Layer']].append(portrayal)

                elif model.right_world[x, y] == model.escalators:
                    portrayal = {'Shape': 'rect',
                                 'x': x,
                                 'y': y,
                                 'w': 1,
                                 'h': 1,
                                 'Filled': 'true',
                                 'Layer': 0,
                                 'Color': ['#ffe6ff', '#ffe6ff'],
                                 'stroke_color': '#ffe6ff'}
                    grid_state[portrayal['Layer']].append(portrayal)

        return grid_state